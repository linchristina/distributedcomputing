/*
	Christina Lin
	Project #3
	A Simple Centralized P2P File Sharing System
	
	Peer.java
	
	This class implements the peer.
	Start the server program before starting the peer
	Usage: java -Djava.rmi.server.codebase=file:///directory_of_program/ Peer <Unique ID> <host>
	
*/

import java.net.*;
import java.rmi.*;
import java.rmi.server.*;
import java.util.*;
import java.io.*;

public class Peer extends UnicastRemoteObject implements PeerInterface {

	static int id;
	static String host;
	static String folder = "";

	static long avgtime = 0;
	static long totaltime = 0;
	static int searches = 0;

	public Peer() throws RemoteException
	{
		super();
	}

	public static void main(String[] args) throws Exception {

		Scanner scan = new Scanner(System.in);

		try {
			id = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.err.println("ID must be an integer");
			System.exit(1);
		}
		host = args[1];
		String peername = "Peer" + id;
		File dir = null;

		do { // Read in path of shared directory
			System.out.print("Enter directory path >> ");
			folder = scan.nextLine();
			dir = new File(folder);
		} while ( !dir.exists() );

		// Create peer server
		Peer clientServer = new Peer();
		try {
			Naming.bind(peername, clientServer);
		} catch (AlreadyBoundException e) {
			System.out.println("[error]: Peer " + id + " is already bound.");
			System.exit(1);
		}
		System.out.println("Client Server running [Peer " + id + "]");

		// Register files in directory
		ServerInterface index = null;
		try {
			index = (ServerInterface)Naming.lookup("rmi://" + host + "/Server");
			register(dir, index);
		} catch (Exception e) {
			System.out.println("[error]: " + e);
			Naming.unbind(peername);
			System.exit(1);
		}

		// Main loop to process user requests
		int op;
		String s = "";
		InputStreamReader stream = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(stream);
		boolean running = true;
		String filename;

		while (running)
		{
			op = 0;
			prompt();
			s = scan.nextLine();
			try {
				op = Integer.parseInt(s);
			} catch (NumberFormatException e) {}

			switch (op) {
			
			case 1: // 1 - Disconnect
				if (searches > 0)
				{
					avgtime = totaltime/searches;
					System.out.println("Average search time: " + avgtime + " ns");
				}
				else
				{
					System.out.println("Average search time: 0 ns");
				}
				running = false;
				break;

			case 2: // 2 - Search for file
				System.out.print("Name of file >> ");
				filename = in.readLine();
				System.out.println();
				search(filename, index);
				break;

			case 3: // 3 - Download file
				System.out.print("Name of file >> ");
				filename = in.readLine();
				System.out.print("\nPeer to download file from >> ");
				try {
					s = scan.nextLine();
					id = Integer.parseInt(s);
				} catch (NumberFormatException e) {
					System.out.println("Peer ID must be integer");
				}
				
				final long dlstart = System.nanoTime();
				final long dlend;
				try {
					getFile(filename, id, index);
				} finally {
					dlend = System.nanoTime();
				}
				final long dltime = dlend - dlstart;
				System.out.println("Download time: " + dltime + " ns");
				break;

			case 4: // List files
				list(dir, index);
				break;

			case 5: // Run evaluation
				System.out.print("Number of searches to run >> ");
				int tests = scan.nextInt();
				System.out.println("...Beginning evaluation...");
				eval(index, tests);
				System.out.println("...Done...");
				break;

			default:
				System.out.println("Invalid command");
				break;
			}
		}
		Naming.unbind(peername);
		System.exit(0);

	}

/* **************************************************
	Display prompt to user
************************************************** */
	private static void prompt()
	{
		System.out.println("\nEnter a command");
		System.out.println("1 - Disconnect");
		System.out.println("2 - Search for a file");
		System.out.println("3 - Download file");
		System.out.println("4 - List files in directory");
		System.out.println("5 - Evaluate performance");
		System.out.print(">> ");
	}

/* **************************************************
	Register client's files
************************************************** */
	private static void register(File dir, ServerInterface server) throws RemoteException
	{
		File[] files = dir.listFiles();
		System.out.println(files.length + " files registered");
		
		if (files.length == 0)
		{
			return;
		}

		for (int i=0; i<files.length; i++)
		{
			server.registry(id, files[i].getName());
		}
		return;
	}

/* **************************************************
	List files current in shared directory
************************************************** */
	private static void list(File dir, ServerInterface index)
	{
		File[] files = dir.listFiles();
		
		System.out.println("Files in your shared directory:");
		for (int i=0; i<files.length; i++) {
			System.out.println(files[i].getName());
		}
	}

/* **************************************************
	Find lists of peers that have the file
************************************************** */
	private static void search(String filename, ServerInterface index) throws RemoteException
	{
		searches++;
		final long searchstart = System.nanoTime();
		final long searchend;

		try {
			List<Integer> peers = new ArrayList<Integer>();
			peers = index.search(filename);

			if (peers == null)
			{
				System.out.println("File not found");
				return;
			}

			Iterator<Integer> it = peers.listIterator();
			System.out.println("File [" + filename + "] found in the following Peer's directories");
			while (it.hasNext())
			{
				System.out.println(it.next());
			}
		} finally {
			searchend = System.nanoTime();
		}
		final long searchtime = searchend - searchstart;
		System.out.println("Search time: " + searchtime + " ns");
		totaltime += searchtime;
	}

/* **************************************************
	obtain(file name)
	Download files
************************************************** */
	public byte[] obtain(String filename) throws IOException
	{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String path = folder + "/" + filename;
		
		File readfile = new File(path);
		if (!readfile.exists()) {
			return null;
		}
		
		int size = (int)readfile.length();
		if (size > Integer.MAX_VALUE) {
			System.out.println("File is too large");
		}
		byte[] bytes = new byte[size];
		DataInputStream stream = new DataInputStream(new FileInputStream(readfile));
		int read = 0;
		int hasRead = 0;
		while (read < bytes.length && (hasRead = stream.read(bytes, read, bytes.length - read)) >= 0) {
			read = read + hasRead;
		}
		
		if (read < bytes.length) {
			System.out.println("Unable to read: [" + readfile.getName() + "]");
		}
		return bytes;
	}

/* **************************************************
	Get file
************************************************** */
	private static void getFile(String filename, int id, ServerInterface server) throws FileNotFoundException, IOException
	{
		PeerInterface peer;
		
		byte[] temp = null;
		
		try {
			peer = (PeerInterface)Naming.lookup("rmi://" + host + "/Peer" + id);
			temp = peer.obtain(filename);
		} catch (NotBoundException e) {
			System.out.println(e);
			return;
		}
		
		if (temp == null) {
			System.out.println("Invalid filename");
			return;
		}

		String pathfile = folder + "/" + filename;
		try {
			FileOutputStream fos = new FileOutputStream(pathfile);
			fos.write(temp);
			fos.close();
			
			server.registry(id, filename);
			System.out.println("display file " + filename);
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException: " + e);
		} catch (IOException e) {
			System.out.println("IOException: " + e);
		}
	}

/* **************************************************
	Evaluation
	Perform evaluation on server
************************************************** */
	private static void eval(ServerInterface index, int tests)
	{
		Random ran = new Random();
		int num;
		
		for (int i=0; i<tests; i++)
		{
			num = ran.nextInt(30)+1;
			String filename = "text" + num + ".txt";
			System.out.println(filename);
			try {
				search(filename, index);
			} catch (RemoteException e) {
				System.out.println(e);
			}
		}
	}

}