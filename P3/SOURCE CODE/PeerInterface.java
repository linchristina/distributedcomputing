/*
	Christina Lin
	Project #3
	A Simple Centralized P2P File Sharing System
	
	PeerInterface.java
	
	This class implements the interface for the Peer.java
	
*/

import java.io.*;
import java.rmi.*;

public interface PeerInterface extends Remote
{
	public byte[] obtain(String file) throws RemoteException, FileNotFoundException, IOException;

}