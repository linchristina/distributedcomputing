/*
	Christina Lin
	Project #3
	A Simple Centralized P2P File Sharing System
	
	Server.java
	
	This class implements the server.
	Start the rmiregistry, then start the server program
	Usage: java -Djava.rmi.server.codebase=file:///directory_of_program/ Server
	
*/

import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import java.util.*;

public class Server extends UnicastRemoteObject implements ServerInterface {

	/* Keep track of the files and the peers */
	HashMap<String, List<Integer>> reg = new HashMap<String, List<Integer>>();

	public Server() throws RemoteException
	{
		super();
	}

	public static void main(String[] args) throws Exception {

		// Create the server
		Server index = new Server();
		try {
			Naming.bind("Server", index);
		} catch (Exception e) {
			System.out.println(e);
			System.exit(0);
		}

		System.out.println("...Server now running...");

		// The program runs run it receives the input "q"
		String s = "";
		System.out.println("Enter q to terminate server");
		while ( !s.equals("q") )
		{
			System.out.print(">> ");
			Scanner scan = new Scanner(System.in);
			s = scan.nextLine();
		}
		Naming.unbind("Server");
		System.exit(0);
	}

/* **************************************************
	registry(peer id, file name)
	registers a peer's files in its shared directory
	to the server where it builds the index for the peer
************************************************** */
	public synchronized boolean registry(Integer id, String file) throws RemoteException
	{

		List<Integer> temp = new ArrayList<Integer>();
		List<Integer> list = new ArrayList<Integer>();
		
		// check if file is in registry
		temp = reg.get(file);
		
		if ( temp == null || temp.isEmpty() ) { // file not in registry
			list.add(id);
			reg.put(file, list);
		} else { // file is in registry
			// check if peer is in registry
			Iterator<Integer> it = temp.listIterator();
			
			while (it.hasNext()) {
				int i = it.next();
				if (i == id) { // file and peer already in list
					return true;
				}
			}
			// file in list but peer is not
			temp.add(id);
			reg.put(file, temp);
		}
		return true;
	}

/* **************************************************
	search(file name)
	searches for the specified file and returns a
	list of all the peers that have the file
************************************************** */
	public List<Integer> search(String file) throws RemoteException
	{
		List<Integer> peers = new ArrayList<Integer>();
		peers = reg.get(file);

		return peers;
	}
}