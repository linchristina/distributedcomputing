/*
	Christina Lin
	Project #3
	A Simple Centralized P2P File Sharing System
	
	ServerInterface.java
	
	This class implements the interface for Server.java
	
*/

import java.rmi.*;
import java.util.*;

public interface ServerInterface extends Remote
{
	public boolean registry(Integer id, String file) throws RemoteException;

	public List<Integer> search(String file) throws RemoteException;

}