/*
 * Christina Lin
 * Project #2 Accelerators
 * Part 1
 * 
 */

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

public class WorkerThread implements Callable<Map <String, Integer>> {
	
	Map<String, Integer> hmap = null;
	String file = null;
	WorkerThread(Map<String, Integer> mapthread, String filename) {
		this.hmap = mapthread;
		this.file = filename;
	}
	
	public Map<String, Integer> call() {
		BufferedReader br;
		StringTokenizer token;
		String line, word;
		int count = 0;
		
		try {
			br = new BufferedReader(new FileReader(file));
			ConcurrentHashMap<String, Integer> hmap = new ConcurrentHashMap<String, Integer>();
			
			// Read in lines from the input file.
			// Tokenize based on blank space.
			// For each token, check if the word is in
			// the hashmap. If it is, update the count,
			// otherwise create the entry for the word.
			while ((line = br.readLine()) != null) {
				token = new StringTokenizer(line, " ");
				
				while (token.hasMoreTokens()) {
					word = token.nextToken();
					if (hmap.get(word) != null) {
						// word is in hashmap
						count = hmap.get(word);
						count++;
						hmap.put(word,  count);
					}
					else {
						// word is not in hashmap
						hmap.put(word,  1);
					}
				}
			}
			br.close();
			return hmap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hmap;
	}
	
}
