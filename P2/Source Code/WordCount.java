/*
 * Christina Lin
 * Project #2 Accelerators
 * Part 1
 * 
 */

import java.util.*;
import java.util.concurrent.*;

public class WordCount {
	
	private static Map<String, Integer> hmap = null;
	
	public static void main (String[] args) {
		String file;
		int numthreads;
		long start = 0, end = 0;
		float totaltime = 0;
		
		start = System.currentTimeMillis();
		if (args.length < 2) {
			System.err.println("Missing arguments");
			System.exit(-1);
		}
		
		numthreads = Integer.parseInt(args[0]);
		file = args[1];
		
		hmap = new ConcurrentHashMap<String, Integer>();
		
		ExecutorService executor = Executors.newFixedThreadPool(numthreads);
		List<Future <Map<String, Integer>>> list = new ArrayList<Future <Map<String, Integer>>>();
		Callable<Map<String, Integer>> worker = new WorkerThread(hmap, file);
		Future<Map<String, Integer>> submit = executor.submit(worker);
		list.add(submit);
		
		// Wait for threads to complete
		executor.shutdown();
		while (!executor.isTerminated()) { }
		end = System.currentTimeMillis();
		totaltime = (float)(end-start) / (float)1000;
		System.out.println(numthreads + " thread(s): " + totaltime + " seconds");
		
/*
		for (Future<Map<String, Integer>> future : list) {
			try {
				for (Map.Entry<String, Integer> entry : future.get().entrySet()) {
					System.out.println(entry.getKey() + "\t" + entry.getValue());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
*/
	}

}
