/*
 * Christina Lin
 * Project #2 Accelerators
 * Part 2
 *
 */

/*
 * Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
 *
 * NVIDIA Corporation and its licensors retain all intellectual property and 
 * proprietary rights in and to this software and related documentation. 
 * Any use, reproduction, disclosure, or distribution of this software 
 * and related documentation without an express license agreement from
 * NVIDIA Corporation is strictly prohibited.
 *
 * Please refer to the applicable NVIDIA end user license agreement (EULA) 
 * associated with this source code for terms and conditions that govern 
 * your use of this NVIDIA software.
 * 
 */


#include "book.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define LIMIT 64//default: 65536
#define ELEMENTS 65536//Default 65536 for your dataset
#define SIZE ELEMENTS*sizeof(unsigned int)
#define CHUNK 1024*sizeof(char)

__host__ __device__ void iterate(unsigned int* table);
__host__ __device__ unsigned int get(unsigned int* table, unsigned int key);
// __device__ unsigned int *tokenize(char *file);
// __device__ int cuda_strlen(const char *str);
// __device__ char *cuda_strtok(char *s, const char *delim);
// __device__ char *cuda_strtok_r(char *s, const char *delim, char **last);
// __device__ unsigned int cuda_atoi(char *s);

__device__ void put(unsigned int* table, unsigned int key){
        atomicAdd(&table[key], 1);
}

__host__ __device__ unsigned int get(unsigned int* table, unsigned int key){
  	unsigned int ret = table[key];//(unsigned long)location2->value;
  	return ret;
}

/*
__global__ void add_to_table( unsigned int *keys, unsigned int* table ) {
  	// get the thread id
  	int tid = threadIdx.x + blockIdx.x * blockDim.x;
  	int stride = blockDim.x * gridDim.x;// total num of threads.

  	while (tid < ELEMENTS) {//stripe
		unsigned int key = keys[tid]; 
		//printf("add_to_table: key = %u, tid = %d, table[tid] = %u\n", key, tid, table[tid]);
    		put(table, key);
    		tid += stride;
  }
	__syncthreads();
}
*/

// copy table back to host, verify elements are there
void verify_table( const unsigned int* dev_table ) {
	unsigned int* host_table;
	host_table = (unsigned int*)calloc(ELEMENTS, sizeof(unsigned int));
	HANDLE_ERROR( cudaMemcpy( host_table, dev_table, ELEMENTS * sizeof( unsigned int ), cudaMemcpyDeviceToHost ) );
  	// iterate(host_table);
  	printf("END VERIFY TABLE\n");
}

__host__ __device__ void iterate(unsigned int* table){
  	for(int i=0; i<ELEMENTS; i++){
    		printf("[%d]: {", i);
		unsigned key = i;
		printf("key = %u ",key);
		printf("value = %u}\n",table[key]);
  	}
}


__global__ void add_to_table( unsigned int *keys, unsigned int *table ) {
	// get the thread id
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	int stride = blockDim.x * gridDim.x;// total num of threads

	if (tid < ELEMENTS) {
		unsigned int key = keys[tid];
		// printf("add_to_table: key = %u, tid = %d, table[tid] = %u\n"    , key, tid, table[tid]);
		put(table, key);
		tid += stride;
	}
	__syncthreads();
}

/*
__device__ int cuda_strlen(const char *str) {
	int len = 0;
	while ((*str++) != '\0')
		len++;

	return len;
}
*/

/*
__device__ unsigned int *tokenize(char *file) {
	char *str = (char *)malloc( sizeof(char) * 10 );
	unsigned int *buffer = (unsigned int *)malloc( CHUNK );
	unsigned int uint = 0;
	int pos = 0;
	char *p = NULL;

	// token strings, convert to unsigned int and copy to array
	for (p = cuda_strtok(file, " "); NULL != p; p = cuda_strtok(NULL, " ")) {
		str = p;
		uint = cuda_atoi( str );
		buffer[pos] = uint;
		pos++;
	}

	return ((unsigned int *)buffer);
}
*/

/*
__device__ unsigned int cuda_atoi(char *s) {
	char *p = s;
	char c;
	int i = 0;

	while (c = *p++) {
		if (c >= '0' && c <= '9') {
			i = i * 10 + (c - '0');
		}
		else
			return 1;
	}
	return i;
}
*/

/*
__device__ char *cuda_strtok(char *s, const char *delim) {
	__shared__ static char *last;
	return cuda_strtok_r(s, delim, &last);
}

__device__ char *cuda_strtok_r(char *s, const char *delim, char **last) {
	char *spanp;
	int c, sc;
	char *tok;

	if (s == NULL && (s = *last) == NULL)
		return NULL;

cont:
	c = *s++;
	for (spanp = (char *)delim; (sc = *spanp++) != 0;) {
		if (c == sc)
			goto cont;
	}

	if (c == 0) {
		*last = NULL;
		return (NULL);
	}
	tok = s - 1;

	for (;;) {
		c = *s++;
		spanp = (char *)delim;
		do {
			if ((sc = *spanp++) == c) {
				if (c == 0)
					s = NULL;
				else
					s[-1] = 0;
				*last = s;
				return (tok);
			}
		} while (sc != 0);
	}

}
*/


int main( int argc, char *argv[] ) {
	FILE *fp;
	char *file;
	int num_blocks = 0;
	int num_threads = 0;

	if (argc < 4) {
		fprintf(stderr, "Usage: %s <input_file> <#blocks> <#threads>\n", argv[0]);
		exit(-1);
	}

	file = argv[1];
	num_blocks = atoi(argv[2]);
	num_threads = atoi(argv[3]);
	fp = fopen(file, "rb");

	if (fp == 0) {
		perror("Cannot open file\n");
		exit(-1);
	}

	printf("Starting main.\n");
	printf("Elements = %u\n", ELEMENTS);

  	unsigned int *dev_keys;
	unsigned int *dev_table;

	char *filebuf = (char *)malloc( CHUNK );
	fread(filebuf, 1, CHUNK, fp);
	char *str = (char *)malloc( sizeof(char) * 10 );
	unsigned int *buffer = (unsigned int *)calloc(1, SIZE);
	unsigned int uint = 0;
	int pos = 0;
	char *p = NULL;
	
	for (p=strtok(filebuf, " "); NULL!=p; p=strtok(NULL, " ")) {
		str = p;
		uint = atoi( str );
		buffer[pos] = uint;
		pos++;
	}

	HANDLE_ERROR( cudaMalloc( (void**)&dev_keys, SIZE ) );
	HANDLE_ERROR( cudaMemcpy( dev_keys, buffer, SIZE, cudaMemcpyHostToDevice ) );

/*
	unsigned int *buffer = (unsigned int*)calloc(1, SIZE);
	for (int i=0; i<ELEMENTS;i++){
                buffer[i]= ELEMENTS - 1;//i;
        }

  // allocate memory on the device for keys and copy to device
	HANDLE_ERROR( cudaMalloc( (void**)&dev_keys, SIZE ) );
  	HANDLE_ERROR( cudaMemcpy( dev_keys, buffer, SIZE, cudaMemcpyHostToDevice ) );
*/

  // Initialize table on device
	HANDLE_ERROR( cudaMalloc( (void**)&dev_table, ELEMENTS * sizeof(unsigned int)) );
	HANDLE_ERROR( cudaMemset( dev_table, 0, ELEMENTS * sizeof(unsigned int) ) );

  	printf("Calling GPU func\n");
  // this launches 60 blocks with 256 threads each, each block is scheduled on a SM without any order guarantees

  	//add_to_table<<<60,256>>>( dev_keys, dev_table);
	add_to_table<<<num_blocks, num_threads>>>( dev_keys, dev_table );
  	cudaDeviceSynchronize();

  	verify_table( dev_table );

  	HANDLE_ERROR( cudaFree( dev_keys ) );
  	free( buffer );

	free(filebuf);
	fclose(fp);
  	return 0;
}
